import { NotificationManager } from "react-notifications";

export const getUserAdminsByGigId = function(gigId, callback) {
  fetch(`/admin-ui/api/gigs/${gigId}/getUserAdmins`, {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  }).then(data => {
    if (data.status !== 200) {
      data.json().then(json => {
        NotificationManager.error(json.error.errmsg);
      });
    } else {
      data.json().then(json => {
        callback(json.user_admins);
      });
    }
  });
};

export const getUserAdmins = async function(gigId) {
  const response = await fetch(`/admin-ui/api/gigs/${gigId}/getUserAdmins`, {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  });
  if (response.status !== 200) {
    NotificationManager.error("Error getting admins");
    return;
  }
  const data = await response.json();
  return data.user_admins;
};

export const getUserById = async function(id) {
  const response = await fetch(`/api/users/${id}`, {
    method: "GET",
    headers: { "Content-Type": "application/json" }
  });
  if (response.status !== 200) {
    NotificationManager.error("Error getting user");
    return;
  }
  const data = await response.json();
  return data.user;
};
