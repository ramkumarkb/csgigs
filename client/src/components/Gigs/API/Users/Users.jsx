import { NotificationManager } from "react-notifications";
import UserProfile from "components/Gigs/Authentication/UserProfile";

export async function login(payload, loadingCallback, redirectCallback) {
  loadingCallback(true);
  const response = await fetch("/api/login", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(payload)
  });

  const data = await response.json();

  loadingCallback(false);
  if (data.error) {
    NotificationManager.error(data.error);
  } else {
    await UserProfile.login(data.user);
    redirectCallback();
  }
}
