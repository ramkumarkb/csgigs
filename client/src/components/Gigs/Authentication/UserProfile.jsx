const UserProfile = (function() {
  const login = async function(user) {
    try {
      const response = await fetch(
        `https://csgigs.com/api/v1/users.getAvatar?username=${
          user.me.username
        }`,
        {
          method: "GET",
          headers: { "Content-Type": "application/json" }
        }
      );
      if (response.status === 200) {
        // const output = await response.json();
        user.me.avatar = response.url;
      }
    } catch (ex) {
      console.log(ex);
    }
    sessionStorage.setItem("user", JSON.stringify(user));
  };
  //test

  const authenticate = function() {
    try {
      const user = JSON.parse(sessionStorage.getItem("user"));
      if (user) {
        return true;
      } else {
        return false;
      }
    } catch (exception) {
      return false;
    }
  };

  const getAuthSet = function() {
    const user = JSON.parse(sessionStorage.getItem("user"));
    const authSet = { token: user.authToken, userId: user.userId };
    return authSet;
  };

  const getUser = function() {
    const user = JSON.parse(sessionStorage.getItem("user"));
    return user;
  };

  return {
    login: login,
    authenticate: authenticate,
    getAuthSet: getAuthSet,
    getUser: getUser
  };
})();

export default UserProfile;
