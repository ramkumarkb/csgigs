import React from "react";
import moment from "moment-timezone";
import Datetime from "react-datetime";

// @material-ui/icons
import Event from "@material-ui/icons/Event";
import Budget from "@material-ui/icons/AttachMoney";
import Cancel from "@material-ui/icons/Cancel";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import TableCell from "@material-ui/core/TableCell";
import FormControl from "@material-ui/core/FormControl";

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/Gigs/CustomInput/CustomInput.jsx";
import Card from "components/Card/Card";
import CardHeader from "components/Card/CardHeader";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody";
import Table from "components/Gigs/Table/Table";
import AutoComplete from "components/Gigs/AutoComplete/AutoComplete";

// dependencies
import { NotificationManager } from "react-notifications";
import CustomSelect from "../../../CustomSelect/CustomSelect";
import CustomRadio from "../../../CustomRadio/CustomRadio";

const style = {
  infoText: {
    fontWeight: "300",
    margin: "10px 0 30px",
    textAlign: "center"
  },
  inputAdornmentIcon: {
    color: "#555"
  },
  inputAdornment: {
    position: "relative"
  },
  readOnly: {
    background: "rgba(200, 200, 200, 0.2)"
  }
};

const formatTimeZone = tz => `(GMT ${moment.tz(tz).format("Z")}) ${tz}`;

const getCurrentTimeZone = () => {
  const currentTimeZone = moment.tz.guess();
  const currentTimeZoneWithOffset = formatTimeZone(currentTimeZone);
  return currentTimeZoneWithOffset;
};

const getAllTimeZones = () => {
  const timeZones = moment.tz.names();
  const timeZonesWithOffset = timeZones.map(tz => formatTimeZone(tz)).sort();
  return timeZonesWithOffset;
};

const allTimeZones = getAllTimeZones();

const dateFromProps = date => (date ? moment(date).format() : "");

class GigDetailsStep extends React.Component {
  constructor(props) {
    super(props);
    // Assuming that if editing an existing, then it is valid
    const isEditingExisting = !!this.props.name;
    this.state = {
      isEditingExisting,
      name: this.props.name || "",
      nameState: isEditingExisting ? "success" : undefined,
      selectedAdmins: this.props.admins || [],
      adminState: isEditingExisting ? "success" : undefined,
      selectedOwner: this.props.owner || "",
      ownerState: isEditingExisting ? "success" : undefined,
      budget: this.props.budget || 0,
      budgetState: isEditingExisting ? "success" : undefined,
      errorMessage: "",
      type: this.props.gigType || "",
      venue: this.props.venue || "",
      address: this.props.address || "",
      region: this.props.region || "",
      timezone: this.props.timeZone || getCurrentTimeZone(),
      startDate: dateFromProps(this.props.startDate),
      endDate: dateFromProps(this.props.endDate),
      format: this.props.format || "",
      maxParticipants: this.props.maxParticipants || "",
      link: this.props.relatedLink || "",
      contact: this.props.contact || "",
      requireRegistration: this.props.registrationRequired === false ? "N" : "Y"
    };
  }

  sendState() {
    return this.state;
  }

  setupTableCells = user => {
    const { classes } = this.props;
    const tableCellClasses = classes.tableCell;
    return (
      <React.Fragment>
        <TableCell colSpan="1" className={tableCellClasses}>
          {user.name}
        </TableCell>
        <TableCell
          colSpan="1"
          className={tableCellClasses}
          style={{ textAlign: "right" }}
        >
          <Cancel className={classes.icon} />
        </TableCell>
      </React.Fragment>
    );
  };

  selectAdmin = admin => {
    const selectedAdmins = this.state.selectedAdmins;
    const existingAdmins = selectedAdmins.filter(
      selectedAdmin => selectedAdmin["_id"] === admin["_id"]
    );
    if (existingAdmins.length >= 1) {
      NotificationManager.error("User " + admin.name + " has been selected");
    } else {
      selectedAdmins.push(admin);
    }
    this.setState({
      selectedAdmins: selectedAdmins,
      adminState: "success"
    });
  };

  deselectAdmin = admin => {
    const selectedAdmins = this.state.selectedAdmins;
    const adminsAfterRemoval = selectedAdmins.filter(
      selectedAdmin => selectedAdmin["_id"] !== admin["_id"]
    );
    if (!adminsAfterRemoval.length) {
      this.setState({
        adminState: ""
      });
    }
    this.setState({
      selectedAdmins: adminsAfterRemoval
    });
  };

  selectOwner = owner => {
    let selectedOwner = this.state.selectedOwner;
    if (selectedOwner && selectedOwner["_id"] === owner["_id"]) {
      NotificationManager.error("User " + owner.name + " has been selected");
    } else if (selectedOwner) {
      NotificationManager.error("Only one owner can be selected");
    } else {
      selectedOwner = owner;
    }
    this.setState({
      ownerState: "success",
      selectedOwner
    });
  };

  selectDate = (momentDate, field) => {
    this.setState({ [field]: momentDate.format() });
  };

  deselectOwner = owner => {
    const selectedOwner = this.state.selectedOwner;
    if (selectedOwner && selectedOwner["_id"] === owner["_id"]) {
      this.setState({
        ownerState: "",
        selectedOwner: undefined
      });
    }
  };

  validateBudget = event => {
    const budget = event.target.value;
    this.setState({
      budget,
      budgetState: budget > 0 ? "success" : "error"
    });
  };

  selectHandle(selectedValue, field) {
    this.setState({ [field]: selectedValue });
  }

  inputHandle(event, field) {
    this.setState({ [field]: event.target.value });
  }

  validateName = event => {
    const name = event.target.value;
    this.setState({ name });
    const reg = /^\w+$/;
    reg.test(name)
      ? this.setState({
          errorMessage: "",
          nameState: "success"
        })
      : this.setState({
          errorMessage: "Special characters in gig name: " + name,
          nameState: "error"
        });
  };

  isValidated() {
    // console.log(this.state);
    if (
      this.state.nameState === "success" &&
      this.state.budgetState === "success" &&
      this.state.adminState === "success" &&
      this.state.ownerState === "success"
    ) {
      return true;
    } else {
      if (this.state.nameState !== "success") {
        this.setState({
          nameState: "error",
          errorMessage: "Field should not be empty."
        });
      }
      if (this.state.budgetState !== "success") {
        this.setState({ budgetState: "error" });
      }
      if (this.state.adminState !== "success") {
        this.setState({ adminState: "error" });
      }
      if (this.state.ownerState !== "success") {
        this.setState({ ownerState: "error" });
      }
    }
    return false;
  }

  render() {
    const { classes } = this.props;
    const {
      isEditingExisting,
      nameState,
      adminState,
      budgetState,
      ownerState,
      selectedAdmins,
      selectedOwner,
      errorMessage,
      type,
      name,
      budget,
      venue,
      address,
      region,
      timezone,
      startDate,
      endDate,
      format,
      maxParticipants,
      link,
      contact,
      requireRegistration
    } = this.state;
    return (
      <GridContainer justify="center">
        <GridItem xs={10} sm={10} md={10} lg={8} align="left">
          <CustomSelect
            labelText={"Type of Gig"}
            id="eventtype"
            items={["Event", "Training", "Announcement"]}
            selectedItem={type}
            inputProps={{
              onChange: selectedValue =>
                this.selectHandle(selectedValue, "type")
            }}
          />
        </GridItem>
        <GridItem xs={10} sm={10} md={10} lg={8} align="left">
          <CustomInput
            success={nameState === "success"}
            error={nameState === "error"}
            labelText={errorMessage || "Name your Gig"}
            id="gigname"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              value: name,
              onChange: this.validateName,
              readOnly: isEditingExisting ? true : false,
              className: isEditingExisting ? classes.readOnly : null,
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <Event className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
            inputType="text"
          />
        </GridItem>
        <GridItem xs={10} sm={10} md={10} lg={8} align="left">
          <CustomInput
            success={budgetState === "success"}
            error={budgetState === "error"}
            labelText={
              <span>
                Brownie Points <small>(required)</small>
              </span>
            }
            id="budgetpoints"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              value: budget,
              onChange: this.validateBudget,
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <Budget className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
            inputType="number"
          />
        </GridItem>
        {type !== "Announcement" && (
          <>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Venue"
                id="venue"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: venue,
                  onChange: event => this.inputHandle(event, "venue")
                }}
                inputType="text"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Address"
                id="address"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: address,
                  onChange: event => this.inputHandle(event, "address")
                }}
                inputType="text"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Region"
                id="region"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: region,
                  onChange: event => this.inputHandle(event, "region")
                }}
                inputType="text"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomSelect
                labelText={"Timezone"}
                id="timezone"
                items={allTimeZones}
                selectedItem={timezone}
                inputProps={{
                  onChange: selectedValue =>
                    this.selectHandle(selectedValue, "timezone")
                }}
              />
            </GridItem>
            <GridItem xs={11} sm={11} md={11} lg={8} align="center">
              <Card>
                <CardHeader color="rose" icon>
                  <CardIcon color="rose">
                    <Event />
                  </CardIcon>
                  <h4
                    className={classes.cardIconTitle}
                    style={{ textAlign: "left", color: "black" }}
                  >
                    Start Date/Time
                  </h4>
                </CardHeader>
                <CardBody>
                  <FormControl fullWidth>
                    <Datetime
                      onChange={momentDate =>
                        this.selectDate(momentDate, "startDate")
                      }
                      inputProps={{
                        value: startDate,
                        placeholder: "Select Date/Time"
                      }}
                    />
                  </FormControl>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={11} sm={11} md={11} lg={8} align="center">
              <Card>
                <CardHeader color="rose" icon>
                  <CardIcon color="rose">
                    <Event />
                  </CardIcon>
                  <h4
                    className={classes.cardIconTitle}
                    style={{ textAlign: "left", color: "black" }}
                  >
                    End Date/Time
                  </h4>
                </CardHeader>
                <CardBody>
                  <FormControl fullWidth>
                    <Datetime
                      onChange={momentDate =>
                        this.selectDate(momentDate, "endDate")
                      }
                      inputProps={{
                        value: endDate,
                        placeholder: "Select Date/Time"
                      }}
                    />
                  </FormControl>
                </CardBody>
              </Card>
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomSelect
                labelText={"Format"}
                id="format"
                items={["Face-to-face", "Audio", "Video", "Telephonic"]}
                selectedItem={format}
                inputProps={{
                  onChange: selectedValue =>
                    this.selectHandle(selectedValue, "format")
                }}
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Max number of participants"
                id="maxParticipants"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: maxParticipants,
                  onChange: event => this.inputHandle(event, "maxParticipants")
                }}
                inputType="number"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Related link"
                id="link"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: link,
                  onChange: event => this.inputHandle(event, "link")
                }}
                inputType="text"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomInput
                labelText="Contact Email"
                id="contact"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  value: contact,
                  onChange: event => this.inputHandle(event, "contact")
                }}
                inputType="email"
              />
            </GridItem>
            <GridItem xs={10} sm={10} md={10} lg={8} align="left">
              <CustomRadio
                labelText={"Require Registration?"}
                id="requireRegistration"
                selectedItem={requireRegistration}
                items={[
                  {
                    key: "Y",
                    value: "Yes"
                  },
                  {
                    key: "N",
                    value: "No"
                  }
                ]}
                inputProps={{
                  onChange: selectedValue =>
                    this.selectHandle(selectedValue, "requireRegistration")
                }}
              />
            </GridItem>
          </>
        )}
        <GridItem xs={10} sm={10} md={10} lg={8} align="left">
          <h4>Assign Owner</h4>
        </GridItem>
        <GridItem xs={11} sm={11} md={11} lg={8} align="center">
          <Card>
            <CardHeader>
              <AutoComplete selectInput={this.selectOwner} />
            </CardHeader>
            <CardBody>
              <Table
                error={ownerState === "error"}
                tableHeight="75px"
                hover
                tableHeaderColor="primary"
                tableData={selectedOwner ? [selectedOwner] : []}
                tableFooter="false"
                notFoundMessage="No owner selected"
                setupTableCells={this.setupTableCells}
                handleTableRowOnClick={this.deselectOwner}
              />
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={10} sm={10} md={10} lg={8} align="left">
          <h4>Assign Gigs Admins</h4>
        </GridItem>
        <GridItem xs={11} sm={11} md={11} lg={8} align="center">
          <Card>
            <CardHeader>
              <AutoComplete selectInput={this.selectAdmin} />
            </CardHeader>
            <CardBody>
              <Table
                error={adminState === "error"}
                tableHeight="200px"
                hover
                tableHeaderColor="primary"
                tableData={selectedAdmins}
                tableFooter="false"
                notFoundMessage="No admins selected"
                setupTableCells={this.setupTableCells}
                handleTableRowOnClick={this.deselectAdmin}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(style)(GigDetailsStep);
