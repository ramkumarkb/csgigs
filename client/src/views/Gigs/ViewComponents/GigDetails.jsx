import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Slide from "@material-ui/core/Slide";
import withMobileDialog from "@material-ui/core/withMobileDialog";

// @material-ui/icons
import Success from "@material-ui/icons/CheckCircle";

// core components
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import CardText from "components/Card/CardText";
import CardHeader from "components/Card/CardHeader";
import CardFooter from "components/Card/CardFooter";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/Gigs/CustomInput/CustomInput";
import PictureUpload from "components/Gigs/CustomUpload/PictureUpload";
import { update } from "components/Gigs/API/Gigs/Gigs";
import GigActions from "components/Gigs/PopupModals/Dialog/GigActions";
import GigDetailsStep from "components/Gigs/Wizard/CreateGigSteps/GigDetailsStep";

import {
  getUserAdmins,
  getUserById
} from "components/Gigs/API/Gigs/UserAdmins";

// dependencies
import Loader from "react-loader-spinner";

// style sheets
import notificationsStyle from "assets/jss/material-dashboard-pro-react/views/notificationsStyle.jsx";

const fullEditFeature = true;

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

class GigDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "working",
      photo: "",
      description: "",
      gigActionsModalOpen: false,
      gigStatus: props.gig.status
    };
    this.upload = React.createRef();
  }

  setStatusState(status) {
    this.setState({
      status: status
    });
  }

  componentWillMount() {
    const { gig } = this.props;
    if (gig.photo) {
      this.setState({
        photo: gig.photo
      });
    }
    if (gig.description) {
      this.setState({
        description: gig.description
      });
    }
  }

  async componentDidMount() {
    if (!fullEditFeature) return;
    const ownerAndAdmins = await Promise.all([
      getUserById(this.props.gig.owner),
      getUserAdmins(this.props.gig._id)
    ]);
    const [owner, admins] = ownerAndAdmins;
    this.setState({ owner, admins });
  }

  componentWillReceiveProps() {
    this.resetState();
  }

  onChangeGigImage(file) {
    this.setState({
      photo: file
    });
  }

  onChangeGigDescription(event) {
    this.setState({
      description: event.target.value
    });
  }

  closeModal() {
    const { hidePopup } = this.props;
    hidePopup("gigDetails");
  }

  resetState() {
    const { gig } = this.props;
    let photo = "";
    let description = "";
    if (gig.photo) {
      photo = gig.photo;
    }
    if (gig.description) {
      description = gig.description;
    }
    if (this.upload) {
      this.upload.resetPhoto(photo);
    }
    this.setState({
      status: "working",
      photo: photo,
      description: description
    });
  }

  confirmGigDetailsEdit = () => {
    const { gig } = this.props;

    if (!this.gigDetailsStep.isValidated()) {
      // Trigger a re-render to update the save button state to be red
      this.setState(this.state);
      return;
    }

    const { photo, description } = this.state;
    const {
      address,
      format: channel,
      contact,
      endDate,
      maxParticipants,
      selectedOwner,
      budget: points_budget,
      region,
      requireRegistration,
      link: relatedLink,
      startDate,
      timezone: timeZone,
      type,
      selectedAdmins,
      venue
    } = this.gigDetailsStep.sendState();

    const owner = selectedOwner._id;
    const user_admins = selectedAdmins.map(x => x._id);

    const payload = {
      address,
      channel,
      contact,
      description,
      endDate,
      maxParticipants,
      owner,
      photo,
      points_budget,
      region,
      registrationRequired: requireRegistration === "Y",
      relatedLink,
      startDate,
      timeZone,
      type,
      user_admins,
      venue
    };
    update(gig._id, payload, this.setStatusState.bind(this));
  };

  openGigActionsPopup() {
    this.setState({
      gigActionsModalOpen: true
    });
  }

  hidePopup = () => {
    this.setState({
      gigActionsModalOpen: false
    });
  };

  onGigStatusChanged = gigStatus => {
    this.setState({
      gigStatus
    });
  };

  render() {
    const { classes, gig } = this.props;
    const {
      photo,
      description,
      status,
      gigActionsModalOpen,
      gigStatus,
      owner,
      admins
    } = this.state;

    const isValid = this.gigDetailsStep
      ? this.gigDetailsStep.isValidated()
      : true;

    return (
      <Card>
        <CardHeader>
          <CardText color="primary" style={{ color: "white" }}>
            {/* <h4 className={classes.cardTitle}></h4> */}
            <h4>{gig.name}</h4>
            <p className="card-category">Gig Status - {gigStatus}</p>
          </CardText>
        </CardHeader>
        <CardBody>
          {fullEditFeature && owner && (
            <GridContainer justify="center">
              <GridItem xs={12} sm={8}>
                <GigDetailsStep
                  name={gig.name}
                  gigType={gig.type}
                  budget={gig.points_budget}
                  venue={gig.venue}
                  address={gig.address}
                  region={gig.region}
                  timeZone={gig.timeZone}
                  startDate={gig.startDate}
                  endDate={gig.endDate}
                  format={gig.channel}
                  maxParticipants={gig.maxParticipants}
                  relatedLink={gig.relatedLink}
                  contact={gig.contact}
                  registrationRequired={gig.registrationRequired}
                  owner={owner}
                  admins={admins}
                  innerRef={node => (this.gigDetailsStep = node)}
                />
              </GridItem>
            </GridContainer>
          )}
          <GridContainer justify="center">
            {status === "loading" ? (
              <GridItem
                xs={10}
                sm={10}
                md={10}
                lg={10}
                style={{ paddingTop: 10, textAlign: "center" }}
              >
                <div style={{ paddingTop: 25 }}>
                  <Loader
                    type="ThreeDots"
                    color="black"
                    height="100"
                    width="100"
                  />
                </div>
              </GridItem>
            ) : null}
            {status === "working" ? (
              <React.Fragment>
                <GridItem
                  xs={10}
                  sm={10}
                  md={10}
                  lg={10}
                  style={{ paddingTop: 10 }}
                >
                  <PictureUpload
                    ref={upload => (this.upload = upload)}
                    onFileChange={this.onChangeGigImage.bind(this)}
                    existingPhoto={photo}
                  />
                </GridItem>
                <GridItem xs={10} sm={10} md={10} lg={10}>
                  <CustomInput
                    labelText={<span>Gig Description</span>}
                    id="taskdescription"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      value: description,
                      multiline: true,
                      onChange: event => this.onChangeGigDescription(event)
                    }}
                    inputType="text"
                  />
                </GridItem>
              </React.Fragment>
            ) : null}
            {status === "success" ? (
              <GridItem
                xs={10}
                sm={10}
                md={10}
                lg={10}
                style={{ textAlign: "center" }}
              >
                <div style={{ paddingTop: 25 }}>
                  <Success
                    className={classes.icon}
                    style={{ height: 100, width: 100, fill: "green" }}
                  />
                  <h4
                    className={classes.modalTitle}
                    style={{ fontWeight: "bold" }}
                  >
                    Details Edited
                  </h4>
                </div>
              </GridItem>
            ) : null}
          </GridContainer>
        </CardBody>
        {status === "working" ? (
          <CardFooter>
            <Button
              onClick={this.confirmGigDetailsEdit}
              className={classes.button + " " + classes.success}
              color={isValid ? "success" : "danger"}
            >
              Save
            </Button>
            <Button
              className={classes.marginRight}
              onClick={this.openGigActionsPopup.bind(this)}
            >
              Actions
            </Button>
            <GigActions
              modalOpen={gigActionsModalOpen}
              hidePopup={this.hidePopup}
              gig={gig}
              onGigStatusChanged={this.onGigStatusChanged}
            />
          </CardFooter>
        ) : null}
      </Card>
    );
  }
}

export default withMobileDialog()(withStyles(notificationsStyle)(GigDetails));
