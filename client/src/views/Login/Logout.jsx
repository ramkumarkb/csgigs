import React from "react";

import UserProfile from "components/Gigs/Authentication/UserProfile";

class Logout extends React.Component {
  componentDidMount() {
    const authenticated = UserProfile.authenticate();
    if (authenticated) {
      sessionStorage.removeItem("user");
      const { history } = this.props;
      history.push({
        pathname: "/login"
      });
    }
  }

  render() {
    return <h1>Logging off from CS Gigs</h1>;
  }
}

export default Logout;
